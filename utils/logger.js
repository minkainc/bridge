const winston = require('winston')
const expressWinston = require('express-winston')
const { LoggingWinston } = require('@google-cloud/logging-winston')

const logger = winston.createLogger({
  level: 'info'
})

if (process.env.NODE_ENV === 'production') {
  logger.add(
    new LoggingWinston({
      service: 'oauth-server',
      version: '0.2.0'
    })
  )
} else {
  logger.add(
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.printf(
          ({ level, message, timestamp }) => `${timestamp} ${level}: ${message}`
        )
      )
    })
  )
}

const loggerMiddleware = expressWinston.logger({
  winstonInstance: logger
})

const errorLoggerMiddleware = expressWinston.errorLogger({
  winstonInstance: logger,
  dumpExceptions: true,
  showStack: true
})

module.exports = {
  loggerMiddleware,
  errorLoggerMiddleware,
  logger
}
