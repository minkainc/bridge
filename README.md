# Bridge 

**Work in progress**

This project has the basic implemantation for bank endpoints.
In this point it has **offline** signing for debit endpoint.

## Make it run

### Install dependencies

    $ npm install

### Run in dev mode

    $ npm run dev

### Run in debug mode

    $ npm run debug

### Run in production mode

    $ npm start

### Config files

In the next file `config/default.js` is necessary to change for the information in favor of the one is needed for the environment.
