const express = require('express')
const wallet = require('@core/wallet')

const router = express.Router()

router.get('/:handle', async (req, res) => {
  const phoneNumber = req.params.handle
  const walletData = await wallet.getWallet(phoneNumber)
  res.send(walletData)
})

module.exports = router
