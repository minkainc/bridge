const Wallet = require('@db/models/wallet')
const { getSignersData } = require('./signer')

/**
 * Get account information
 *
 * @param {string} phoneNumber wallet handle
 * @returns {Array} signer information
 */
const getWallet = async phoneNumber => {
  try {
    const { entityData } = await Wallet.findOne({ handle: phoneNumber })
    return getSignersData(entityData.signer)
  } catch (error) {
    return []
  }
}

module.exports = {
  getWallet
}
