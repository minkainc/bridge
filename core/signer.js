const Signer = require('@db/models/signer')

/**
 * Get one signer information and format it
 *
 * @param {string } asynchandle signer handle
 * @returns {Object} formated signer
 */
const getSigner = async handle => {
  const { entityData } = await Signer.findOne({ handle })
  return formatSignerData(entityData)
}

/**
 * Get a list of formated signers
 *
 * @param {Array} signers list of signers
 * @returns {Array} formated signers
 */
const getSignersData = signers => {
  return Promise.all(signers.map(getSigner))
}

const formatSignerData = ({ handle, labels }) => ({
  handle,
  firstName: labels.firstName,
  lastName: labels.lastName,
  bankName: labels.bankName,
  bankAccountNumber: obfuscateField(labels.bankAccountNumber),
  bankAccountType: labels.bankAccountType
})

/**
 * Show last characters of a string
 *
 * @param {string} field
 * @param {number} visibleLen=4 amount of characters to show
 * @returns {string | undefined}
 */
const obfuscateField = (field, visibleLen = 4) => {
  try {
    field = field.toString()
    return `****${field.substr(visibleLen * -1)}`
  } catch {
    return undefined
  }
}

module.exports = {
  getSignersData
}
