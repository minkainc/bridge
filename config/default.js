const path = require('path')

module.exports = {
  port: 3000,
  projectUrl: 'https://achtin-dev.minka.io/v1/',
  rootPath: path.resolve(__dirname, '..')
}
