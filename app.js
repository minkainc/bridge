require('@db/datastore')
const express = require('express')
const bodyParser = require('body-parser')

const wallet = require('@routes/wallet')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/wallet', wallet)

module.exports = app
