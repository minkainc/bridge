const { resolve } = require('path')
const { Gstore, instances } = require('gstore-node')
const { Datastore } = require('@google-cloud/datastore')
const config = require('config')

const { logger } = require('@utils/logger')

const gstore = new Gstore({ errorOnEntityNotFound: false })

const MULTIREG_PROJECTS = ['ach-tin-stg', 'ach-tin-prd']

const createDSInstance = () => {
  const { NODE_ENV, GOOGLE_CLOUD_PROJECT } = process.env
  if (
    NODE_ENV === 'production' &&
    MULTIREG_PROJECTS.includes(GOOGLE_CLOUD_PROJECT)
  ) {
    logger.info(`[ds:connect] to ${GOOGLE_CLOUD_PROJECT}-multireg`)
    return new Datastore({
      projectId: `${GOOGLE_CLOUD_PROJECT}-multireg`,
      keyFilename: resolve(config.get('rootPath'), 'keyfile.json')
    })
  } else {
    logger.info('[ds:connect] using default configuration')
    return new Datastore()
  }
}

const datastore = createDSInstance()
gstore.connect(datastore)

instances.set('bridge', gstore)
