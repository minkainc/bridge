const { instances } = require('gstore-node')

const gstore = instances.get('bridge')

const KIND = 'Wallet'

const walletSchema = new gstore.Schema({
  handle: {
    type: String
  },
  labels: {
    type: Object
  }
})

module.exports = gstore.model(KIND, walletSchema)
