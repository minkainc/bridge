const { instances } = require('gstore-node')

const gstore = instances.get('bridge')

const KIND = 'Signer'

const signerSchema = new gstore.Schema({
  handle: {
    type: String
  },
  labels: {
    type: Object
  }
})

module.exports = gstore.model(KIND, signerSchema)
